package miu.edu.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties;
import org.springframework.context.annotation.Bean;

@EnableDiscoveryClient
@SpringBootApplication
public class APIGatewayApplication {
    @Bean
    public DiscoveryClientRouteDefinitionLocator discoveryClientRouteDefinitionLocator(ReactiveDiscoveryClient rdc,
                                                                                       DiscoveryLocatorProperties dlp) {
        dlp.setLowerCaseServiceId(true); // Optional
        return new DiscoveryClientRouteDefinitionLocator(rdc, dlp);
    }
    public static void main(String[] args) {
        SpringApplication.run(APIGatewayApplication.class, args);
    }
}
